﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursachNYSS
{
    public class Path
    {
        public string Directory { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string FullPath { get; set; }
    }
}
