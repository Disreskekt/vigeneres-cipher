﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CursachNYSS
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public static char[] alphabet = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Deshifr_Click(object sender, RoutedEventArgs e)
        {
            Path path = new Path();
            if (!PathBuilder(path))
            {
                return;
            }
            if ((bool)CheckBoxDeshifrOut.IsChecked)
            {
                ReadFile(path, "Deshifr");
            }
            else if ((bool)CheckBoxDeshifrIn.IsChecked)
            {
                SaveFile(path, "Deshifr");
            }
            else if ((bool)CheckBoxDeshifrLocal.IsChecked)
            {
                TextBoxAfter.Text = DeshifrMethod(TextBoxBefore.Text, path.Key);
            }
            else
            {
                MessageBox.Show("Выберите режим дешифровки");
            }
        }

        private void Shifr_Click(object sender, RoutedEventArgs e)
        {
            Path path = new Path();
            if (!PathBuilder(path))
            {
                return;
            }
            if ((bool)CheckBoxShifrOut.IsChecked)
            {
                ReadFile(path, "Shifr");
            }
            else if ((bool)CheckBoxShifrIn.IsChecked)
            {
                SaveFile(path, "Shifr");
            }
            else if ((bool)CheckBoxShifrLocal.IsChecked)
            {
                TextBoxAfter.Text = ShifrMethod(TextBoxBefore.Text, path.Key);
            }
            else
            {
                MessageBox.Show("Выберите режим шифровки");
            }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Path path = new Path();
            if (!PathBuilder(path))
            {
                return;
            }
            SaveFile(path, "Save");
        }

        public string DeshifrMethod(string before, string key)
        {
            string after = "";
            int counter = 0;
            int wordNumber = 0;
            int keyWordNumber = 0;
            for (int i = 0; i < before.Length; i++)
            {
                if (alphabet.Contains(Char.ToLower(before[i])))
                {
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (alphabet[j] == Char.ToLower(before[i]))
                        {
                            wordNumber = j;
                            break;
                        }
                    }
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (alphabet[j] == Char.ToLower(key[counter % key.Length]))
                        {
                            keyWordNumber = j;
                            break;
                        }
                    }
                    if (wordNumber >= keyWordNumber)
                    {
                        after += alphabet[wordNumber - keyWordNumber];
                        counter++;
                    }
                    else
                    {
                        wordNumber += 33;
                        after += alphabet[wordNumber - keyWordNumber];
                        counter++;
                    }
                }
                else
                {
                    after += before[i];
                }
            }
            return after;
        }
        public string ShifrMethod(string before, string key)
        {
            string after = "";
            int counter = 0;
            int wordNumber = 0;
            int keyWordNumber = 0;
            for (int i = 0; i < before.Length; i++)
            {
                if (alphabet.Contains(Char.ToLower(before[i])))
                {
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (alphabet[j] == Char.ToLower(before[i]))
                        {
                            wordNumber = j;
                            break;
                        }
                    }
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (alphabet[j] == Char.ToLower(key[counter % key.Length]))
                        {
                            keyWordNumber = j;
                            break;
                        }
                    }
                    if (wordNumber + keyWordNumber < 33)
                    {
                        after += alphabet[wordNumber + keyWordNumber];
                        counter++;
                    }
                    else
                    {
                        wordNumber -= 33;
                        after += alphabet[wordNumber + keyWordNumber];
                        counter++;
                    }
                }
                else
                {
                    after += before[i];
                }
            }
            return after;
        }

        private void CheckBoxDeshifrIn_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxDeshifrIn.IsChecked == CheckBoxDeshifrOut.IsChecked)
            {
                CheckBoxDeshifrOut.IsChecked = !CheckBoxDeshifrIn.IsChecked;
            }
            if (CheckBoxDeshifrIn.IsChecked == CheckBoxDeshifrLocal.IsChecked)
            {
                CheckBoxDeshifrLocal.IsChecked = !CheckBoxDeshifrIn.IsChecked;
            }
        }

        private void CheckBoxDeshifrOut_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxDeshifrOut.IsChecked == CheckBoxDeshifrIn.IsChecked)
            {
                CheckBoxDeshifrIn.IsChecked = !CheckBoxDeshifrOut.IsChecked;
            }
            if (CheckBoxDeshifrOut.IsChecked == CheckBoxDeshifrLocal.IsChecked)
            {
                CheckBoxDeshifrLocal.IsChecked = !CheckBoxDeshifrOut.IsChecked;
            }
        }

        private void CheckBoxDeshifrLocal_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxDeshifrLocal.IsChecked == CheckBoxDeshifrIn.IsChecked)
            {
                CheckBoxDeshifrIn.IsChecked = !CheckBoxDeshifrLocal.IsChecked;
            }
            if (CheckBoxDeshifrLocal.IsChecked == CheckBoxDeshifrOut.IsChecked)
            {
                CheckBoxDeshifrOut.IsChecked = !CheckBoxDeshifrLocal.IsChecked;
            }
        }

        private void CheckBoxShifrIn_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxShifrIn.IsChecked == CheckBoxShifrOut.IsChecked)
            {
                CheckBoxShifrOut.IsChecked = !CheckBoxShifrIn.IsChecked;
            }
            if (CheckBoxShifrIn.IsChecked == CheckBoxShifrLocal.IsChecked)
            {
                CheckBoxShifrLocal.IsChecked = !CheckBoxShifrIn.IsChecked;
            }
        }

        private void CheckBoxShifrOut_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxShifrOut.IsChecked == CheckBoxShifrIn.IsChecked)
            {
                CheckBoxShifrIn.IsChecked = !CheckBoxShifrOut.IsChecked;
            }
            if (CheckBoxShifrOut.IsChecked == CheckBoxShifrLocal.IsChecked)
            {
                CheckBoxShifrLocal.IsChecked = !CheckBoxShifrOut.IsChecked;
            }
        }

        private void CheckBoxShifrLocal_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxShifrLocal.IsChecked == CheckBoxShifrIn.IsChecked)
            {
                CheckBoxShifrIn.IsChecked = !CheckBoxShifrLocal.IsChecked;
            }
            if (CheckBoxShifrLocal.IsChecked == CheckBoxShifrOut.IsChecked)
            {
                CheckBoxShifrOut.IsChecked = !CheckBoxShifrLocal.IsChecked;
            }
        }

        public void ReadFile(Path path, string s)
        {
            try
            {
                using (FileStream fileStream = File.OpenRead(path.FullPath))
                {
                    byte[] arr = new byte[fileStream.Length];
                    fileStream.Read(arr, 0, arr.Length);
                    TextBoxBefore.Text = System.Text.Encoding.Default.GetString(arr);
                }
                if (s == "Deshifr")
                {
                    TextBoxAfter.Text = DeshifrMethod(TextBoxBefore.Text, path.Key);
                }
                if (s == "Shifr")
                {
                    TextBoxAfter.Text = ShifrMethod(TextBoxBefore.Text, path.Key);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void SaveFile(Path path, string s)
        {
            if (File.Exists(path.FullPath))
            {
                FileExistsWindow fileExistsWindow = new FileExistsWindow();
                fileExistsWindow.ShowDialog();
                if (fileExistsWindow.DialogResult == false)
                {
                    return;
                }
                else if (fileExistsWindow.DialogResult == null)
                {
                    MessageBox.Show("Ошибка. OkOrCancel = null");
                    return;
                }
            }
            try
            {
                if (s == "Deshifr")
                {
                    TextBoxAfter.Text = DeshifrMethod(TextBoxBefore.Text, path.Key);
                }
                if (s == "Shifr")
                {
                    TextBoxAfter.Text = ShifrMethod(TextBoxBefore.Text, path.Key);
                }
                using (FileStream fileStream = File.Create(path.FullPath))
                {
                    byte[] arr = System.Text.Encoding.Default.GetBytes(TextBoxAfter.Text);
                    fileStream.Write(arr, 0, arr.Length);
                }
                MessageBox.Show("Файл успешно сохранен");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool PathBuilder(Path path)
        {
            if (TextBoxDirectory.Text != "Рабочий стол")
            {
                if (Directory.Exists(TextBoxDirectory.Text))
                {
                    path.Directory = TextBoxDirectory.Text;
                }
                else
                {
                    MessageBox.Show("Директории не существует");
                    return false;
                }
            }
            else
            {
                path.Directory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            }
            if (TextBoxName.Text.Contains("\\") || TextBoxName.Text.Contains("\"") || TextBoxName.Text.Contains("/") || TextBoxName.Text.Contains("|") || TextBoxName.Text.Contains("?") || TextBoxName.Text.Contains("*") || TextBoxName.Text.Contains(":") || TextBoxName.Text.Contains(">") || TextBoxName.Text.Contains("<"))
            {
                MessageBox.Show("Имя файла не должно содержать следующих знаков:\n\\ / : * ? \" < > |");
                return false;
            }
            else
            {
                path.Name = TextBoxName.Text;
            }
            if (TextBoxKey.Text == "" || TextBoxKey.Text == null)
            {
                MessageBox.Show("Ключ не должен быть пустым");
                return false;
            }
            foreach (var item in TextBoxKey.Text.ToLower())
            {
                if (!alphabet.Contains(item))
                {
                    MessageBox.Show("Ключ должен содержать только символы русского алфавита");
                    return false;
                }
            }
            path.Key = TextBoxKey.Text;
            path.FullPath = path.Directory + "\\" + path.Name + ".txt";
            return true;
        }
    }
}